/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartupds.merge.common;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;


/**
 *
 * @author mafragias
 */
public class Utils {
    
    /**
     * Returns the types contained in the files, selected to be merged.
     * @param files : array of files
     * @return
     */
    public static HashSet<String> getFileTypes(String[] files){
        HashSet<String> types =  new HashSet<>();
//        System.out.println(String.join(" - ", files));
        for (String file : files)
            types.add(file.substring(file.lastIndexOf(".")+1));

        types.forEach(x->System.out.println(x));
        return types;
    }
    
    /**
     * Checks the contents of a directory an returns a lists of files inside it.
     * @param folder a File
     * @return list of files
     */
    public static String[] listFilesForFolder(final File folder) {
        ArrayList<String> filePaths = new ArrayList<>(); 
        for (final File fileEntry : folder.listFiles()) {
            if (fileEntry.isDirectory()) {
                listFilesForFolder(fileEntry);
            } else {
                System.out.println(folder.getAbsoluteFile() + "\\" + fileEntry.getName());
                filePaths.add(folder.getAbsoluteFile() + "\\" + fileEntry.getName());
            }
        }
        return filePaths.toArray(new String[filePaths.size()]);
    }
}
