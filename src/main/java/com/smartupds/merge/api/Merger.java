/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartupds.merge.api;

/**
 *
 * @author mafragias
 */
public interface Merger {
    
    /**
     * Method to override and use for merging files.
     */
    public void merge();

    /**
     * Method to override and use for merging files in standard size.
     * @param parseDouble : size of the file
     */
    public void mergeToSize(double parseDouble);
}
