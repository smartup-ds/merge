/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartupds.merge;

import com.smartupds.merge.impl.XMLMerger;
import com.smartupds.merge.impl.DatasetMerger;
import com.smartupds.merge.api.Merger;
import com.smartupds.merge.common.Resources;
import com.smartupds.merge.common.Utils;
import com.smartupds.merge.impl.PlainMerger;
import java.io.File;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 *
 * @author mafragias
 */
public class Main {
    static CommandLineParser parser = new DefaultParser();
    static Options options = new Options();

    public static void main(String[] args){
        createOptions();
        try {
//            args = new String[]{"-f","./workspace/Basic1961/Basic1961_part_0.xml ./workspace/Basic1961/Basic1961_part_1.xml"};
//            args = new String[]{"-f","./workspace/ulan/ulan_part_0.tsv ./workspace/ulan/ulan_part_1.tsv"};
//            args = new String[]{"-f","C:\\Users\\mafragias\\Documents\\PHAROS\\Enrichment\\enrichment_ulan_nationality_part_1.n3 C:\\Users\\mafragias\\Documents\\PHAROS\\Enrichment\\enrichment_ulan_nationality_part_2.n3"};
//            args = new String[]{"-d","./workspace/ulan/"};
//            args = new String[]{"-d","./workspace/Basic1961/"};
//            args = new String[]{"-d","C:\\Users\\mafragias\\Documents\\PHAROS\\Pharos-9-2020\\typesAAT"};
//            args = new String[]{"-d","C:\\Users\\mafragias\\Documents\\PHAROS\\Enrichment\\moredata"};
//            args = new String[]{"-f","C:\\Users\\mafragias\\Documents\\Data_For_Instance_Matching\\Matched\\Artists\\artists_zeri_ulan_part_1.ttl C:\\Users\\mafragias\\Documents\\Data_For_Instance_Matching\\Matched\\Artists\\artists_zeri_ulan_part_2.ttl"};
//            args = new String[]{"-d","C:\\Users\\mafragias\\Documents\\PHAROS\\Migration\\RS-MigratedFields"};
//            args = new String[]{"-d","C:\\Users\\mafragias\\Documents\\PHAROS\\Migration\\fields\\rs-fields-migrated-3-12-2020\\"};
            args = new String[]{
                "-d","D:\\Harvester\\Output\\DE-Y3 transformed pack1\\example_of_merging",
                 "-s","1"
            };
//            String[] fake_args = {"-d","./workspace/plain/","-s","0.01"};
            CommandLine line = parser.parse( options, args );
            handleCommandLine(line);
        } catch( ParseException exp ) {
            printOptions();
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, "Reason : ".concat(exp.getMessage()));
        }
    }
    
    private static void createOptions() {
        Option file = new Option("f",true,"Files to merge : -f [file_0] [file_1] ... [file_n]");
        file.setArgs(Option.UNLIMITED_VALUES);
        file.setValueSeparator(' ');
        file.setArgName("file");
        Option directory = new Option("d",true,"Directory containing files to merge : -d [directory]");
        directory.setArgName("directory");
        Option size = new Option("s",true,"File size (MB) to limit the merging process.");
        size.setArgName("size");
        options.addOption(file);
        options.addOption(directory);
        options.addOption(size);
    }

    private static void handleCommandLine(CommandLine line) {
        Logger.getLogger(Main.class.getName()).log(Level.INFO, "Merging Process Started");
        Merger merger = (Merger) null;
        String[] files = null;
        
        if (line.hasOption("f"))
            files = line.getOptionValues("f");
        else if (line.hasOption("d"))
            files = Utils.listFilesForFolder(new File(line.getOptionValue("d")));
        HashSet<String> types = Utils.getFileTypes(files);
        if (files.length>1 && types.size()==1){
            if (types.contains(Resources.TSV) || types.contains(Resources.CSV))
                merger = new DatasetMerger(files);
            else if (types.contains(Resources.TRIG) || types.contains(Resources.TTL) || types.contains(Resources.NT) || types.contains(Resources.N3))
                merger = new PlainMerger(files);
            else if (types.contains(Resources.XML) || types.contains(Resources.RDF))
                merger = new XMLMerger(files);
            else
                merger = new PlainMerger(files);
        } else {
            if (types.size()>1)
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, "Reason : ".concat("Conflicting file types."));
            else
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, "Reason : ".concat("Select more than one files or a directory."));
        }
        if (line.hasOption("s"))
            merger.mergeToSize(Double.parseDouble(line.getOptionValue("s")));
        else
            merger.merge();
        Logger.getLogger(Main.class.getName()).log(Level.INFO, "Merging Process Finished");
        
    }

    private static void printOptions(){
        String header = "\nChoose from options below:\n\n";
        String footer = "\nParsing failed.";
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("java -jar PhotoSimilarity-1.0-assembly.jar", header, options, footer, true);
    }
 
    
    
    
}
