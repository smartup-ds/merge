/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartupds.merge.impl;

import com.smartupds.merge.api.Merger;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mafragias
 */
public class PlainMerger implements Merger{
    private String[] files;
    
    public PlainMerger(String[] files) {
        this.files = files;
    }
    
    @Override
    public void merge() {
        try {
            String mergeFilename = new File(files[0]).getParent()+"/"
                    +LocalDateTime.now().toString().replace(":", "-").replace(".", "-")
                    +"_merged_files"+files[0].substring(files[0].lastIndexOf("."));
            Logger.getLogger(DatasetMerger.class.getName()).log(Level.INFO, "Creating merged file : ".concat(mergeFilename));
            OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(mergeFilename, true), "UTF-8");
            int fileCounter = 0;
            for (String file:files){
                double percent = (double) (fileCounter+1)*100/files.length;
                Logger.getLogger(DatasetMerger.class.getName()).log(Level.INFO, "Merging File : ".concat(file));
                Logger.getLogger(DatasetMerger.class.getName()).log(Level.INFO, "Merging Process at{0} %", percent);
                BufferedReader reader = new BufferedReader( new InputStreamReader(new FileInputStream(file), "UTF8"));
                String row = "";
                while((row = reader.readLine())!=null){
                        writer.append(row+"\n");
                }
                fileCounter++;
            }
            writer.close();
        } catch (FileNotFoundException | UnsupportedEncodingException ex) {
            Logger.getLogger(DatasetMerger.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DatasetMerger.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public void mergeToSize(double size){
        size = size*1024*1024;
        try {
            String mergeFilename = new File(files[0]).getParent()+"/"
                    +LocalDateTime.now().toString().replace(":", "-").replace(".", "-")
                    +"_merged_files_"+0+files[0].substring(files[0].lastIndexOf("."));
            Logger.getLogger(DatasetMerger.class.getName()).log(Level.INFO, "Creating merged file : ".concat(mergeFilename));
            OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(mergeFilename, true), "UTF-8");
            double mergedSize = 0;
            int counter = 1;
            for (String file:files){
                Logger.getLogger(DatasetMerger.class.getName()).log(Level.INFO, "Merging File : ".concat(file));
                BufferedReader reader = new BufferedReader( new InputStreamReader(new FileInputStream(file), "UTF8"));
                String row = "";
                while((row = reader.readLine())!=null){
                        writer.append(row+"\n");
                }
                
                mergedSize += new File(file).length();
                if (mergedSize>=size){
                    writer.close();
                    mergeFilename = new File(files[0]).getParent()+"/"
                            +LocalDateTime.now().toString().replace(":", "-").replace(".", "-")
                            +"_merged_files_"+counter+files[0].substring(files[0].lastIndexOf("."));
                    Logger.getLogger(DatasetMerger.class.getName()).log(Level.INFO, "Creating merged file : ".concat(mergeFilename));
                    writer = new OutputStreamWriter(new FileOutputStream(mergeFilename, true), "UTF-8");
                    mergedSize=0;
                    counter++;
                }
            }
            writer.close();
        } catch (FileNotFoundException | UnsupportedEncodingException ex) {
            Logger.getLogger(DatasetMerger.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DatasetMerger.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
   
}
