/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartupds.merge.impl;

import com.smartupds.merge.api.Merger;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

/**
 *
 * @author mafragias
 */
public class XMLMerger implements Merger {

    private String[] files;

    public XMLMerger(String[] files) {
        this.files = files;
    }

    @Override
    public void merge() {
        try {
            String mergeFilename = new File(files[0]).getParent() + "/"
                    + LocalDateTime.now().toString().replace(":", "-").replace(".", "-")
                    + "_merged_files" + files[0].substring(files[0].lastIndexOf("."));
            Logger.getLogger(DatasetMerger.class.getName()).log(Level.INFO, "Creating merged file : ".concat(mergeFilename));
            int fileCounter = 0;
            SAXReader reader = new SAXReader();
            reader.setEncoding("UTF-8");
            Logger.getLogger(DatasetMerger.class.getName()).log(Level.INFO, "Merging File : ".concat(files[0]));
            Document first_doc = reader.read(new FileInputStream(files[0]));
            Element first_rootElement = first_doc.getRootElement();
            List<Element> first_elements = first_rootElement.elements();
            for (String file : files) {
                double percent = (double) (fileCounter + 1) * 100 / files.length;
                Logger.getLogger(DatasetMerger.class.getName()).log(Level.INFO, "Merging File : ".concat(file));
                Logger.getLogger(DatasetMerger.class.getName()).log(Level.INFO, "Merging Process at{0} %", percent);
                if (fileCounter != 0) {
                    Document current_doc = reader.read(new FileInputStream(file));
                    Element current_rootElement = current_doc.getRootElement();
                    List<Element> current_elements = current_rootElement.elements();
                    if (first_rootElement.getName().equals(current_rootElement.getName())) {
                        for (Element current_element : current_elements) {
                            if (first_elements.get(0).getName().equals(current_element.getName())) {
                                first_rootElement.add(current_element.detach());
                            }
                        }
                    }
                }
                fileCounter++;
            }
            OutputFormat format = OutputFormat.createPrettyPrint();
            XMLWriter xmlwriter = new XMLWriter(new OutputStreamWriter(new FileOutputStream(mergeFilename), "UTF-8"), format);
            xmlwriter.write(first_doc);
            xmlwriter.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(XMLMerger.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException | DocumentException ex) {
            Logger.getLogger(XMLMerger.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(XMLMerger.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void mergeToSize(double size) {
        try {
            size = size * 1024 * 1024;

            String mergeFilename = new File(files[0]).getParent() + "/"
                    + LocalDateTime.now().toString().replace(":", "-").replace(".", "-")
                    + "_merged_files_" + 0 + files[0].substring(files[0].lastIndexOf("."));
            Logger.getLogger(DatasetMerger.class.getName()).log(Level.INFO, "Creating merged file : ".concat(mergeFilename));

            double mergedSize = 0;
            int fileCounter = 0;
            int current_file_counter=0;

            SAXReader reader = new SAXReader();
            reader.setEncoding("UTF-8");
            Logger.getLogger(DatasetMerger.class.getName()).log(Level.INFO, "Merging File : ".concat(files[0]));

            Document first_doc = reader.read(new FileInputStream(files[0]));
            Element first_rootElement = first_doc.getRootElement();
            List<Element> first_elements = first_rootElement.elements();
            for (String file : files) {
                    
                Logger.getLogger(DatasetMerger.class.getName()).log(Level.INFO, "Merging File : ".concat(file));
                if (fileCounter != 0) {

                    Document current_doc = reader.read(new FileInputStream(file));
                    Element current_rootElement = current_doc.getRootElement();
                    List<Element> current_elements = current_rootElement.elements();
                    if (first_rootElement.getName().equals(current_rootElement.getName())) {
                        for (Element current_element : current_elements) {
                            if (first_elements.get(0).getName().equals(current_element.getName())) {
                                first_rootElement.add(current_element.detach());

                            }
                        }
                    }
                }
                mergedSize += (new File(file)).length();

                if (mergedSize >= size || (fileCounter+1)==files.length ) {
                    mergeFilename = new File(files[0]).getParent() + "/"
                            + LocalDateTime.now().toString().replace(":", "-").replace(".", "-")
                            + "_merged_files_" + fileCounter + files[0].substring(files[0].lastIndexOf("."));
                    Logger.getLogger(DatasetMerger.class.getName()).log(Level.INFO, "Creating merged file : ".concat(mergeFilename));
                    OutputFormat format = OutputFormat.createPrettyPrint();
                    XMLWriter xmlwriter = new XMLWriter(new OutputStreamWriter(new FileOutputStream(mergeFilename), "UTF-8"), format);
                    xmlwriter.write(first_doc);
                    xmlwriter.close();
                    first_doc = reader.read(new FileInputStream(files[current_file_counter]));
                    first_rootElement = first_doc.getRootElement();
                    first_elements = first_rootElement.elements();
                    mergedSize = 0;
                    
                }
                fileCounter++;
                current_file_counter++;
                
            }

        } catch (DocumentException ex) {
            Logger.getLogger(XMLMerger.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(XMLMerger.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(XMLMerger.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(XMLMerger.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}


