# README #

Merger Applet that supports xml, rdf, tsv, csv, trig, ttl, nt or n3 files. There is also an option to add size in order to limit the merging process.

### Run ###

```bash
$ java -jar Merge-0.1-assembly.jar -f [file_0] [file_1] ... [file_n]
```
or 
```bash
$ java -jar Merge-0.1-assembly.jar -d [directory]
```
or 
```bash
$ java -jar Merge-0.1-assembly.jar -d [directory] -s [size_in_MB]
```